/*

  Модули в JS
  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import

  Так как для экспорта и импорта нету родной поддержки в браузерах, то
  нам понадобится сборщик или транспалер который это умеет делать.
  -> babel, webpack, rollup

  На сегодняшний день - самое полулярное решение, это вебпак!

  npm i webpack webpack-cli

  Установка и config-less настройка

  "scripts": {
    "cli": "webpack ./application/index.js --output-path ./public/js --output-filename bundle.js --mode development --color --watch"
  }

*/
  // `webpack
  //     ./application/index.js
  //     --output-path ./public/js
  //     --output-filename bundle.js
  //     --mode development
  //     --color
  //     --watch
  // `;

/*

  npm run cli
  Затестим - в консоли наберем команду webpack

*/

  // import imports from './imports';


import Government from '../classworks/singleton'
import DeepFreeze from '../classworks/objectfreeze'


//Object deep freeze (task)
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };


  let FarGalaxy = DeepFreeze(universe);
      // FarGalaxy.good.push('javascript'); // true - тоже замораживается потому что по typeof массив выдаётся как object...
      // FarGalaxy.something = 'Wow!'; // false
      // FarGalaxy.evil.humans = [];   // false



//Singleton, government

Government.addLaw(1, 'new law', 'textextetxt');
Government.addLaw(2, 'new law2', 'textextetxt');
Government.addLaw(3, 'new law3', 'textextetxt');
Government.readConstitution();
Government.readLaw(1);
Government.showCitizensSatisfaction();
Government.runEvent();
Government.showBudget();
Government.showCitizensSatisfaction();

