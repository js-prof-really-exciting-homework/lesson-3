/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

const _data = {
  laws: [],
  budget: 1000000,
  citizensSatisfaction: 0
}

const Government = {
  addLaw: (id, name, description) =>{    
    function Law (id, name, description){
      this.id = id;
      this.name = name;
      this.description = description;

      _data.laws.push(this);
      _data.citizensSatisfaction = _data.citizensSatisfaction - 10;
    }
    let law = new Law(id, name, description);
  },

  readConstitution: () => console.log([..._data.laws]),

  readLaw: id => console.log( _data.laws.find( l => l.id === id) ),

  showCitizensSatisfaction: () => console.log ( _data.citizensSatisfaction ),

  showBudget: () => console.log ( _data.budget ),

  runEvent: () => {
    _data.budget = _data.budget - 50000;
    _data.citizensSatisfaction = _data.citizensSatisfaction + 5;
  },
}

export default Government;